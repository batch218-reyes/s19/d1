// console.log("Hello World");

// What are conditional statements?
// Conditional statements allow us to control the flow of our program and it allow us to run statement/instruction based on the condition.

// if statement
// Executes a statement if a specified condition is true

/*
	Syntax:
	if(condition) {
		code block / statement;
	}
*/

let numA = 0;

if (numA < 0) {
	console.log("hello"); // nothing will show beacause it is false 
}
// the code block inse the if statement will not work if ti is  doesnt satisify the condition

if (numA == 0) {
	console.log("hello");
}
// the result of te expression  must result t "true", else it will not run te staement inside.

console.log(numA < 0) //false
console.log(numA == 0) // true

let city = "New York";

if (city == "New York"); {
console.log("Welcome to New yourk City");
}

// else if clause
/*
	-Executes a statements if previous condition/s are false and if the specified condition is true
	=the "else if" clause is optional and can be added to capture additional conditions to change the flow of the program
*/

// city = "New York";
if(city === "New York") {
	console.log("Welcome to New york City");
}

else if (city === "Tokyo") {
	console.log("welcome to tokyo, Japan")
}







let numH =0;

if (numH<0){ // returned false
	console.log("The number is negative");
}

else if(numH>0){ // returned false
	console.log("the number is positive");
}

else if (numH == 0) {
	console.log("The value of zero");
}

// Alternative code 



if (numH<0){ // returned false
	console.log("The number is negative");
}

else if(numH>0){ // returned false
	console.log("the number is positive");
}

else{
	console.log("The number of zero");
}

// else statement
/*
  -Executes a statement if all other conditions are false.
  -the 'else' statement is optional and can be added to capture any other result to chane the flow of the prgrame
*/



// city = "New York";
if(city === "New York") {
	console.log("Welcome to New york City");
}

else if (city === "Tokyo") {
	console.log("welcome to tokyo, Japan")
}
else{
	console.log("City is not included in the list")
}

// let	usermane ="admin";
// let pasword ="admin1234"

// if (usermane ="admin") {
// 	console.log ("correct username");
// }

// else if (pasword ="admin1234") {
// 	console.log ("correct password");
// }
// else{
// 	console.log("wrong username or password");
// }

// if(username === "admin" && pasword == "admin1234"){
// 	console.log("sucessful login")
// }

// else{
// 	console.log ("Wong username or password");
// }


let message = "No message";
	console.log(message);

	function determineTyphooneIntensity(windSpeed){

		if(windSpeed < 30){
			return "Not a typhoon yet."
		}
		else if(windSpeed <= 61){
			return "Tropical depression detected."
		}
		else if (windSpeed >= 62 && windSpeed <= 88){
			return "Tropical storm detected."
		}
		else if(windSpeed >= 89 && windSpeed <= 117){
			return "Severe Tropical storm deteceted";
		}
		else{
			return "Typhoon detected."
		}
	}

	message = determineTyphooneIntensity(110);
	console.log(message);

	//  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
	if(message === "Severe Tropical storm deteceted"){
		console.warn(message);
	}

// [SECTION] turthy and false
/*
	-in javascipt a "truthy" value that is considered true when encounted in a bolean context
	-false
		1. false
		2. 0
		3. ""
		4. null
		5. undefined // let number;
		6.NaN(Not a Number)

*/
let ismarried = true;
// truthy examples:
if (true) {
	console.log("truthy");
}

if (1) {
	console.log("truthy");
}

if ([]) {
	console.log("truthy");
}

// Falsy Examples
if (false) {
	console.log("falsy");
}
if (0) {
	console.log("falsy");
}
if (undefined) {
	console.log("falsy");
}
if (ismarried) {
	console.log("truthy");
}

// [SECTION] Conditional (ternary) Operator
/*
	- The Conditiona (ternary) operator takes in three operands
*/

let ternaryResult = (1 < 18)? true : false ;
console.log("result of ternary operator: " + ternaryResult)

// multiple statement excution using ternary operator

// let name;

// function isOfLegalAge(){
// 	name = "John";
// 	return "you are in legal age";

// }

// function underAge(){
// 	name ="Jane";
// 	return "You are under age limit ";
// }

// let age = parseInt(prompt("what is your age? "));
// console.log(age);

// let legalAge = (age>=18)? isOfLegalAge() : underAge();
// console.log("Return of Ternary Operator in function " + legalAge + ',' +name);

// [SECTION] Switch Statemens

let day = prompt("what day of the week is it today"). toLowerCase();
console.log(day); //display Input

switch(day) {
	case "monday": // cant place condition
		console.log("the color of the day is red"); //display Output
		break;
	case "tuesday":
		console.log("the color of the day is orange");
		break;
	case "wednesday":
		console.log("the color of the day is yellow");
		break;
	case "thursday":
		console.log("the color of the day is green");
		break;
	case "Friday":
		console.log("the color of the day is blue");
		break;
	case "saturday":
		console.log("the color of the day is indigo");
		break;
	case "sunday":
		console.log("the color of the day is violet");
		break;

	default:
		console.log("Please Input a date");
		break;	
}

// let userName = promt("age: ");
// age = parseInt(age);

// switch(age){
// 	case 18:
// 		console.log"Age is valid"
// 	case 17:
// 		console.log"Age is valid"
// 	case 19:
// 		console.log"Age is valid"
// 	case 18:
// 		console.logs"Give Freebies"
// 	case	

// }


// [SECTION] Try-Catch-Finally Statement

let codeErrorMessage = "Code Error";
							//parameter
function showInstensitiyAlert(windSpeed) {
	try {
		// codes / code  block ot try
		alerat(determineTyphooneIntensity(windSpeed)); //this is the mistake
	}

	catch(error){
		// is used to access information realating to an error object
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}

}

showInstensitiyAlert(56);

console.log("Sample output after try-catch-finaly block")